from django.db import models
from users.models import CustomUser
from ckeditor.fields import RichTextField
from simple_history.models import HistoricalRecords

class BaseModel(models.Model):
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    history = HistoricalRecords(inherit=True)
    class Meta:
       abstract = True


class SportType(BaseModel):
    name = models.CharField(max_length=300)
    
    class Meta:
        verbose_name = "Sport tur"
        verbose_name_plural = "Sport turlari"
    def __str__(self):
        return self.name
    
class ClassStudent(BaseModel):
    name = models.CharField(max_length=300)
    
    class Meta:
        verbose_name = "Talaba sinf"
        verbose_name_plural = "Talaba sinflari"
    def __str__(self):
        return self.name
    
    
    
class Teacher(BaseModel):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    class Meta:
        verbose_name = "O'qituvchi"
        verbose_name_plural = "O'qituvchilar"
    
    
class Subject(BaseModel):
    name = models.CharField(max_length=300)
    teacher = models.ForeignKey(Teacher, on_delete=models.CASCADE)
    class Meta:
        verbose_name = "Mavzu"
        verbose_name_plural = "Mavzular"
    def __str__(self):
        return self.name
    
class Lesson(BaseModel):
    subject = models.ForeignKey(Subject, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    order = models.PositiveSmallIntegerField(default=0)
    class Meta:
        verbose_name = "Dars"
        verbose_name_plural = "Darslar"
    def __str__(self):
        return self.name
    
class ResursFile(BaseModel):
    file = models.FileField(upload_to='filetest/')
    
    class Meta:
        verbose_name = "Resurs Fayl"
        verbose_name_plural = "Resurs Fayllar"
  
    
class Resurs(BaseModel):
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    files = models.ManyToManyField(ResursFile)
    class Meta:
        verbose_name = "Resurs"
        verbose_name_plural = "Resurslar"
    def __str__(self):
        return self.name

    
    
class Group(BaseModel):
    subject = models.ManyToManyField(Subject)
    sport_type = models.ForeignKey(SportType, on_delete=models.CASCADE)
    name = models.CharField(max_length=300)
    class_student = models.ForeignKey(ClassStudent,on_delete=models.CASCADE)
    
    
    
class ContentType(BaseModel):
    name  = models.CharField(max_length=300)
    def __str__(self):
        return self.name
    
    
class Content(BaseModel):
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    name  = models.CharField(max_length=300)
    order = models.PositiveSmallIntegerField()
    content_type = models.ForeignKey(ContentType, on_delete=models.CASCADE)
    file = models.FileField(null=True,blank=True)
    content = RichTextField(null=True,blank=True)
    
    def __str__(self):
        return self.name

class Student(BaseModel):
    user = models.ForeignKey(CustomUser, on_delete=models.CASCADE)
    group = models.ManyToManyField(Group)
    class_student = models.ForeignKey(ClassStudent,on_delete=models.CASCADE)
    class Meta:
        verbose_name = "Talaba"
        verbose_name_plural = "Talabalar"
    
class Attedance(BaseModel):
    lesson = models.ForeignKey(Lesson, on_delete=models.CASCADE)
    student = models.ForeignKey(Student, on_delete=models.CASCADE)
    date = models.DateField()