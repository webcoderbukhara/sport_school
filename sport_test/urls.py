
from django.urls import path
from .views import *
urlpatterns = [
    path('', test_lectures, name='test_lectures'),
    path('<int:id>/', test, name='test'),
    path('lecture/<int:id>/', test_lectures_result, name='test_lectures_result'),
     path('teachers/', teachers, name="teachers"),
    path('teacher/<int:id>/detail/', teacher_detail, name="teacher_detail"),
    path('groups/', groups, name="groups"),
    # path('group_detail/<int:id>/detail/', group_detail, name="group_detail"),g
    path('students/', students, name="students"),
    # path('students_in_debt/', students_in_debt, name="students_in_debt"),
    path('student/<int:id>/detail/', student_detail, name="student_detail"),
    path('add-student/', add_student, name="add_student"),
    path('add-group/', add_group, name="add_group"),
    path('add-teacher/', add_teacher, name="add_teacher"),
    path('students-state/', students_state, name="students_state"),
    # path('teacher-effectiveness/', teacher_effectiveness, name="teacher_effectiveness"),
    # path('left-students/', left_students, name="left_students"),
    # path('course-effectiveness/', course_effectiveness, name="course_effectiveness"),
    # path('students-days/', students_days, name="students_days"),
    # path('courses/', courses, name="courses"),
    # path('course_detail/<int:id>/', course_detail, name="course_detail"),
    path('attendance_report/', attendance_report, name="attendance_report"),

] 
