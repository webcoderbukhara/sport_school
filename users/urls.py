
from django.urls import path

from .views import *

urlpatterns = [
   
    path('login/', log_in, name="login"),
    path('reset/', reset, name="reset"),
    path('profile/', profile, name="profile"),
    path('logout/', logout_view, name="logout"),
]