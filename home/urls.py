from django.urls import path
from .views import *


urlpatterns = [
    path('', home, name='home'),
    path('contact', contact, name='contact'),
    path('about', about, name='about'),
    path('gallery', gallery, name='gallery'),
    path('dashboard/', dashboard, name='dashboard'),
    path('login/', log_in, name='login'),
]