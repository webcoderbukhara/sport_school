from django.db import models
from django.contrib.auth.base_user import AbstractBaseUser
from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.models import BaseUserManager
from django.utils.translation import gettext_lazy as _

from simple_history.models import HistoricalRecords




class CustomUserManager(BaseUserManager):
    def create_user(self, username, password, **extra_fields):
        if not username:
            raise ValueError("username fields is required")
        extra_fields.setdefault('is_staff', True)
        extra_fields.setdefault('is_active', True)
        user = self.model(username=username, **extra_fields)
        user.set_password(password)
        user.save()
        return user

    def create_superuser(self, username, password, **extra_fields):
        """
        Create and save a SuperUser with the given email and password.
        """
        extra_fields.setdefault('is_superuser', True)

        return self.create_user(username, password, **extra_fields)
# Create your models here.
    def __str__(self):
        return self.name
class CustomUser(AbstractBaseUser, PermissionsMixin):
    username = models.CharField(
        _('username'), max_length=200, unique=True
    )
    full_name = models.CharField(max_length=500,null=True,blank=True)
    image = models.ImageField(null=True, blank=True,upload_to='users/',default='static/assets/img/user.png')
    address = models.CharField(max_length=500,null=True,blank=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_staff = models.BooleanField(default=False)
    is_superuser = models.BooleanField(default=False)
    is_active = models.BooleanField(default=True)
    last_seen = models.DateTimeField(blank=True, null=True)
    history = HistoricalRecords(inherit=True)
    is_director = models.BooleanField(default=False)
    is_teacher = models.BooleanField(default=False)
    is_student = models.BooleanField(default=False)
    is_admin = models.BooleanField(default=False)
    class Meta:
        ordering = ("created_at",)

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    objects = CustomUserManager()
    def __str__(self):
        return self.username
    
    def get_absolute_url(self):
        return f'/users/{self.id}'

    def get_update_url(self):
        return f'/users/{self.id}/update'

    def get_delete_url(self):
        return f'/users/{self.id}/delete'



        
        
