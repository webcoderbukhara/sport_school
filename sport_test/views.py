from django.shortcuts import redirect, render
from .models import ControlTest, Test, UserControlTestResult
from dashboard.models import Attedance, Group, SportType, Student, Teacher
from django.shortcuts import redirect, render
from .models import *
from django.contrib.auth.decorators import login_required


# Create your views here.
def test_lectures(request):
    if request.user.is_authenticated:
        context = {
            'SportType': SportType.objects.all(),
            'results': UserControlTestResult.objects.filter(user=request.user).all()
        }
       
        if request.method == "POST":
                post = request.POST
                les = post.get('SportType', False)
                sport_type = SportType.objects.get(id=int(les))
                if les:
                  return redirect('test', id=les)
                  
                
                else:
                    context['error'] = 'baza hali tayyor emas'
        return render(request, 'test/test_lectures.html', context)
    else: return(redirect('log_in'))

def test_lectures_result(request,id):
    if request.user.is_authenticated:
        context = {
            'r': UserControlTestResult.objects.get(id=id)
        }
        
        return render(request, 'test/test_lectures_result.html', context)
    else: return(redirect('log_in'))

def test(request,id):
    if request.user.is_authenticated:
        # try: 
            sport_type = SportType.objects.get(id=id)
            context = {
            'testes' : Test.objects.filter(sport_type=sport_type).all().order_by('?')[:25],
            'lecture' : sport_type
            } 
            if request.method == 'POST':
                result = UserControlTestResult.objects.create(user=request.user,sport_type=sport_type)
                for p,value in request.POST.items():
                        if p != "csrfmiddlewaretoken":
                            question = Test.objects.get(id=p)
                            ts = ControlTest.objects.create(question=question)
                            ts.answer = value
                            if question.ans == value:
                                ts.is_true = True
                            ts.save()
                            result.tests.add(ts)
                            result.save()
                return redirect('test_lectures_result', id=result.id)
            return render(request, 'test/test.html',context)
        # except:
        #     return redirect('home')
    else:
        return redirect('home')
    
    

# Create your views here.


@login_required
def teachers(request):
    context = {
        'teachers': Teacher.objects.all(),
    }
    if request.method == 'POST' and (request.user.is_director or request.user.is_admin):
        delete_group = request.POST.get('delete_group',False)
        if delete_group:
            dg = Teacher.objects.get(id=int(delete_group))
            dg.delete()
    return render(request, 'crm/teachers.html',context)


@login_required
def teacher_detail(request,id):
    context = {
        'teachers': True,
        't': Teacher.objects.get(id=id)
        
    }
    return render(request, 'crm/teacher_detail.html',context)


@login_required
def groups(request):
    context = {
        'groups': Group.objects.all(),
        
    }
    if request.method == 'POST':
        delete_group = request.POST.get('delete_group',False)
        if delete_group:
            dg = Group.objects.get(id=int(delete_group))
            dg.delete()
    return render(request, 'crm/groups.html',context)

@login_required
def attendance_report(request):
    attedence = Attedance.objects.filter(is_has=False).all()
    if request.method == 'POST':
        post = request.POST
        date = post.get('date',False)
        group = post.get('group',False)
        begin_date = post.get('begin_date',False)
        if group:
            attedence = attedence.filter(day__group_id=int(group)).all()
        if begin_date:
            attedence = attedence.filter(day__date__gte=begin_date).all()
        if date:
            attedence = attedence.filter(day__date=date).all()
    
    context = {
        'groups': Group.objects.all(),
        'attedence' : attedence
        
    }
    if request.method == 'POST':
        delete_group = request.POST.get('delete_group',False)
        if delete_group:
            dg = Group.objects.get(id=int(delete_group))
            dg.delete()
    return render(request, 'crm/attendance_report.html',context)
@login_required
def students(request):
    context = {
        'students': Student.objects.all(),
        
    }
    return render(request, 'crm/students.html',context)
@login_required
def student_detail(request,id):
    student =  Student.objects.get(id=id)
    s = get_student_info(student)
   
    context = {
        'students': True,
        't': student,
        'groups':  Group.objects.all(),
        's':s
    }
    if request.method == 'POST':
        post  = request.POST
        new_group = post.get('new_group',False)
        if new_group:
            student.group.add(int(new_group))
            context['message'] = "Talaba guruhga biriktirildi"
            return redirect('student_detail',id=student.id)

        delete_student = post.get('delete_student',False)
        if delete_student:
            student.delete()
            return redirect('students')
    
    return render(request, 'crm/student_detail.html',context)

@login_required
def add_student(request):
    context = {
        'addstudent': True,
        'group': Group.objects.all(),
        
    }
    if request.method == 'POST':
        post = request.POST
        full_name = post.get('full_name',False)
        begin_date = post.get('begin_date',False)
        
        password = post.get('password',False)
        group = post.get('group',False)
        
      
        
       
        
        if not CustomUser.objects.filter(phone=phone).exists():
            if phone and full_name and gender and birthday:
                user = CustomUser.objects.create(phone=phone, full_name=full_name,birthday=birthday,gender_id=int(gender))
                if password:
                    user.set_password(password)
                else: user.set_password(phone)
                
                user.is_student = True
                user.save()
                st = Student.objects.create(user=user)
                st.name = full_name
                if comment:
                    st.comment
                if group:
                    st.group.add(int(group)) 
                if begin_date:
                    st.begin_date = begin_date
                if education:
                    st.education = education
                if phones:
                    p = Phone.objects.create(name=phone)
                    p.save()
                    st.phones.add(p)
                if parent_type and parent_phone and parent_full_name:
                    pr = Parent.objects.create(type_id=int(parent_type),phone = parent_phone, full_name = parent_full_name)
                    pr.save()
                    st.parent.add(pr)
                context['message'] = f"Talaba {user.full_name} tizimga kiritildi"
                    
            else: context['error'] = "Ma'lumotlar to'liq kiritilmadi"
        else: context['error'] = "Bunday telefon raqamga oldin foydalanuvchi ro'yxatdan o'tgan"
    return render(request, 'crm/add-student.html',context)

@login_required
def add_group(request):
    context = {
        'addgroup': True,
        'course': Cource.objects.all(),
        'teacher': Teacher.objects.all(),
        
        
    }
    if request.method == 'POST':
        post = request.POST
        name = post.get('name',False)
        cource = post.get('cource',False)
        teacher = post.get('teacher',False)
        room = post.get('room',False)
        start_time = post.get('start_time',False)
        begin_date = post.get('begin_date',False)
        week_days = post.getlist('week_days',False)
        if name and cource and teacher and room and start_time and week_days:
            gr = Group.objects.create(name=name,cource_id=int(cource),teacher_id=int(teacher),room_id=int(room),start_time_id=int(start_time))
            if week_days:
                day = Days.objects.create(name=name)
                for i in week_days:
                    day.week_days.add(int(i))
                    day.save()
                gr.days = day
            if begin_date:
                gr.begin_date = begin_date
            gr.save()
            context['message'] = f"{gr.name} guruhi tizimga qo'shildi"
        else: context['error'] = "Ma'lumotlar to'liq kiritilmadi"
        
        
    return render(request, 'crm/add-group.html',context)

@login_required
def add_teacher(request):
    context = {
        'addteacher': True,
        
        
    }
    if request.method == 'POST':
        post = request.POST
        full_name = post.get('full_name',False)
        password = post.get('password',False)
        if full_name:
            if CustomUser.objects.filter(full_name=full_name).exists():
                context['error'] = "Bunday foydalanuvchi mavjud"
            else:
                user = CustomUser.objects.create(phone=phone,full_name=full_name,birthday=birthday,gender_id=int(gender))
                if password:
                    user.set_password(password)
                else: user.set_password(phone)
                user.is_teacher = True
                user.save()
                teacher = Teacher.objects.create(user=user)
                teacher.save()
                context['message'] = f"O'qituvchi {user.full_name} qo'shildi"

        else: context['error'] = "Ma'lumotlar to'liq kiritilmadi"
    return render(request, 'crm/add-teacher.html',context)

@login_required
def students_state(request):
    context = {
        'studentsstate': True,
        
    }
    return render(request, 'crm/students-state.html',context)



# @login_required
# def group_detail(request,id):
#     if Group.objects.filter(id=id).exists():
#         g = Group.objects.get(id=id)
#         students = []
#         for s in g.students():
#             sl = get_student_info_group(s,g)
#             students.append(sl)

        
#         context = {
#         'group_detail': True,
#         'g': g,
#         'today': date.today(),
#         'students': students        
#     }
        
#         if request.method == 'POST':
#             post = request.POST
#             ad_date = post.get('ad_date',False)
#             if ad_date:
#                 ad_date = Day.objects.get(id=int(ad_date))
#                 context['ad_date'] = ad_date.date
#                 context['date_id'] = ad_date
#             st_ad = post.getlist('st_ad',False)
#             ad_date2 = post.get('ad_date2',False)
#             if ad_date2:
#                  o_ss = Attedence.objects.filter(day_id=int(ad_date2)).all()
#                  if len(o_ss) > 0:
#                     for o in o_ss:
#                         o.is_has = True
#                         o.save()
#             if st_ad and ad_date2:
               
#                 for s in st_ad:
#                     st,_ = Attedence.objects.get_or_create(day_id=int(ad_date2),student_id=int(s))
#                     st.is_has = False
#                     st.save()
#             leave_student = post.get('leave_student',False)
#             if leave_student:
#                 ls = Student.objects.get(id=int(leave_student))
#                 ls.group.remove(g)
#                 return redirect('group_detail', id=g.id)
                    
#     else: return redirect('home')
#     return render(request, 'crm/group_detail.html',context)




# @login_required
# def students_days(request):
#     students = []
#     for s in Student.objects.all():
#         sl = get_student_info(s)
#         students.append(sl)    
    
#     context = {
#         'students': students,
#     }
#     return render(request, 'crm/students_days.html',context)
# @login_required
# def courses(request):
#     context = {
#         'cources': Cource.objects.all(),
#         'ducaration': Ducaration.objects.all(),
#     }
#     if request.method == 'POST':
#         post = request.POST
#         name = post.get('name',False)
#         price = post.get('price',False)
#         code = post.get('code',False)
#         ducaration = post.get('ducaration',False)
#         month = post.get('month',False)
#         comment = post.get('comment',False)
#         if name and price and ducaration and month:
#             c = Cource.objects.create(name=name,price=price,ducaration_id=ducaration,month=month)
#             if code:
#                 c.code = code
#             if comment:
#                 c.comment = comment
#             c.save()
#             context['message'] = f"Kurs {c.name} tizimga qo'shildi"
#             return redirect('courses')
#         else:
#             context['error'] = "Ma'lumotlar to'liq kiritilmadi"
#     return render(request, 'crm/courses.html',context)

# @login_required
# def rooms(request):
#     context = {
#         'rooms': Room.objects.all(),
#     }
#     if request.method == 'POST':
#         post = request.POST
#         name = post.get('name',False)
#         if name:
#             r = Room.objects.create(name=name)
#             r.save()
#             context['message'] = f"Xona {r.name} tizimga qo'shildi"
#             return redirect('rooms')
#         else:
#             context['error'] = "Ma'lumotlar to'liq kiritilmadi"
#         dele = request.POST.get('delete_room_id',False)
#         if dele:
#             c = Room.objects.get(id=int(dele))
#             c.delete()
#             return redirect('rooms')
       
#     return render(request, 'crm/rooms.html',context)

# @login_required
# def course_detail(request,id):
#     c = Cource.objects.get(id=id)
#     context = {
#         'c': c, 
#     }
#     if request.method == 'POST':
#         dele = request.POST.get('delete_cource',False)
#         if dele:
#             c.delete()
#             return redirect('courses')
            
#     return render(request, 'crm/course_detail.html',context)

