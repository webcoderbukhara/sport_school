
from django.db import models
from users.models import CustomUser
from dashboard.models import ClassStudent, SportType, BaseModel
from ckeditor.fields import RichTextField


class Test(BaseModel):
    question = RichTextField()
    ans = RichTextField()
    ans1 = RichTextField()
    ans2 = RichTextField()
    ans3 = RichTextField()
    student_class = models.ForeignKey(ClassStudent,on_delete=models.CASCADE,null=True)
    sport_type = models.ForeignKey(SportType,on_delete=models.CASCADE)
    class Meta:
        verbose_name = "Test"
        verbose_name_plural = "Testlar"
    def answers(self):
        context = [
            { 'name':self.ans, 'is_true':True },
            { 'name':self.ans1, 'is_true':False },
            { 'name':self.ans2, 'is_true':False },
            { 'name':self.ans3, 'is_true':False },
            ]
        return list(context)

class ControlTest(BaseModel):
    question = models.ForeignKey(Test, on_delete=models.CASCADE)
    answer = RichTextField()
    is_true = models.BooleanField(default=False)
    
    def __str__(self) -> str:
        return self.question.question
    class Meta:
        verbose_name = "Test javob"
        verbose_name_plural = "Testlarni javoblari"
    

class UserControlTestResult(BaseModel):
    sport_type = models.ForeignKey(SportType, on_delete=models.CASCADE )
    student_class = models.ForeignKey(ClassStudent, on_delete=models.CASCADE,null=True)
    user = models.ForeignKey(CustomUser,on_delete=models.CASCADE)
    date = models.DateTimeField(auto_now_add=True)
    tests = models.ManyToManyField(ControlTest)
    
    def result(self):
        if (self.tests.filter(is_true=True).count()) > 0:
            r = (self.tests.filter(is_true=True).count()*100) / self.tests.count()
            return '{:.2f}'.format(r)
        else: return 0
    
    def is_trues(self):
        return self.tests.filter(is_true=True).count()


    def str(self) -> str:
        return self.user.get_full_name()

    class Meta:
        ordering = ['-date']
    class Meta:
        verbose_name = "Talaba natijasi"
        verbose_name_plural = "Talabalar natijasi"

