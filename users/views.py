from django.shortcuts import redirect, render
from django.contrib.auth.decorators import login_required
from django.contrib.auth import login, authenticate, logout

from users.models import CustomUser
# Create your views here.
def log_in(request):
    
    context = {
        'login': True,
        
    }
    if request.method == 'POST':
        post = request.POST
        username = post.get('username',False)
        password = post.get('password',False)
        if CustomUser.objects.filter(phone=username).exists():
                        user = CustomUser.objects.get(phone=username)
                        
                        if (user.check_password(password) or user.password == password) and user:
                            login(request,user)
                            return redirect('home')
                        else:
                           context['err'] = 'Login yoki parol xato'
        else: 
            context['err'] = 'Bunday foydalanuvchi mavjud emas'
            context['username'] = username
    
    return render(request, 'auth/login.html',context)
@login_required
def reset(request):
    context = {
        'reset': True,
        
    }
    return render(request, 'auth/reset-password.html',context)
@login_required
def profile(request):
    context = {
        'profile': True,
        
    }
    return render(request, 'auth/profile.html',context)



@login_required
def logout_view(request):
    logout(request)
    
    return redirect('login')