from django.contrib import admin
from .models import *
# Register your models here.

admin.site.register(SportType)
admin.site.register(ClassStudent)
admin.site.register(Teacher)
admin.site.register(Subject)
admin.site.register(Lesson)
admin.site.register(ResursFile)
admin.site.register(Resurs)
admin.site.register(Student)
admin.site.register(Group)
admin.site.register(ContentType)
admin.site.register(Content)
admin.site.register(Attedance)