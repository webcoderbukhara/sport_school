from django.shortcuts import render
from django.http import JsonResponse
from django.shortcuts import redirect, render
from django.contrib.auth.models import User
from django.contrib.auth import login, authenticate, logout

# Create your views here.
def home(request):
    return render(request,'home.html')

def contact(request):
    return render(request,'contact.html')

def about(request):
    return render(request,'about.html')

def gallery(request):
    return render(request,'gallery.html')


def dashboard(request):
    return render(request,'Dashboard/home.html')


def log_in(request):
    context = {}
    if request.method == "POST":
        print(request.POST)
        post = request.POST
        user = post.get('username', False)
        password = post.get('password', False)
        user = authenticate(username=user, password=password)
        if user is not None:
            login(request,user)
            return redirect('home')
        else: 
            context['error'] = 'Foydalanuvchi topilmadi'
    return render(request,'login.html',context)