from django.contrib import admin
from simple_history.admin import SimpleHistoryAdmin


from .models import *
# Register your models here.
@admin.register(CustomUser)
class Admin(SimpleHistoryAdmin):
    list_display = [f.name for f in CustomUser._meta.fields]
    history_list_display = ["status"]
